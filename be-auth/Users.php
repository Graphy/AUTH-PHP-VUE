<?php
require __DIR__.'/config.php';
require __DIR__.'/Auth.php';

$userData = [];
$result;

if(isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
}

if(isset($_REQUEST['password'])) {
    $passw = $_REQUEST['password'];
    $userData['password'] = $passw;
}

if(isset($_REQUEST['email'])) {
    $email = $_REQUEST['email'];
    $userData['email'] = $email;
}


class Users
{

    /**
     * @return response
     */
    public function logInUser($userToLogin)
    {

        $config = new Config();
        $configuration =  $config->configuration;

        $auth = new Auth(
            $configuration['db_dsn'],
            $configuration['db_user'],
            $configuration['db_pass']
        );

        $response = $auth->logInUser($userToLogin['email'], $userToLogin['password']);
        return $response;
    }

    /**
     * @return response
     */
    public function saveUsers($usersToSave)
    {
        $config = new Config();
        $configuration =  $config->configuration;

        $auth = new Auth(
            $configuration['db_dsn'],
            $configuration['db_user'],
            $configuration['db_pass']
        );

        $passHash = password_hash($usersToSave['password'], PASSWORD_DEFAULT);

        $response = $auth->saveUsers($usersToSave['email'], $passHash);
        return $response;
    }

    /**
     * @return response
     */
    public function getCookie() {
        if (isset($_COOKIE['gekkehenkie'])) {
            // get data from cookie for local use
            $pieces = explode(",", $_COOKIE["gekkehenkie"]); // holds an hash and a number
            $tokenHash = explode("=", $pieces[0])[1]; // hash
            $usrId = explode("=", $pieces[1])[1]; // number

            $config = new Config();
            $configuration =  $config->configuration;

            $auth = new Auth(
                $configuration['db_dsn'],
                $configuration['db_user'],
                $configuration['db_pass']
            );

            $response = $auth->AuthCookie($tokenHash, $usrId);
            return $response;
        } else {
            // no cookie at all
            $response = false;
            return $response;
        }
    }
}

if(!empty($action)) {
    $call = new Users();
   $result = $call->$action($userData);
} else {
    $result = "I don't know what to do with myself!";
}

header('Access-Control-Allow-Origin: http://localhost:8080');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Content-Type');

if (!is_string($result)) {
    echo json_encode($result);
} else {
    echo $result;
}
