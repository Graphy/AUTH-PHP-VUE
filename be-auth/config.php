<?php 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ( ini_set( 'display_errors', '1' ) === false ) {
    throw new \Exception( 'Unable to set display_errors.' );  
}

class Config
{
    public $configuration = array(
        'db_dsn' => 'mysql:dbname=AUTH-Base;host=127.0.0.1',
        'db_user' => 'root',
        'db_pass' => '',
    );
}
