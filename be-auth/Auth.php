<?php

class Auth
{
    private $pdo;
    private $dbDsn;
    private $dbUser;
    private $dbPass;

    public function __construct($dbDsn, $dbUser, $dbPass) {
        $this->dbDsn = $dbDsn;
        $this->dbUser = $dbUser;
        $this->dbPass = $dbPass;
    }

    /**
     * @return PDO
     */

    private function getPDO()
    {
        if(null === $this->pdo) {
            $pdo = new PDO($this->dbDsn, $this->dbUser, $this->dbPass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }

        return $this->pdo;
    }

    /**
     * @return usersArray
     */

    private function getUsers() {
        $pdo = $this->getPDO();
        $statement = $pdo->prepare('SELECT * FROM users');
        $statement->execute();
        $usersArray = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $usersArray;
    }

    /**
     * @return result
     */
    private function checkExistenceOfUser($email) {
        $pdo = $this->getPDO();
        $existingUsr;
        $result;

        //Query statement with placeholder
        $query = 'SELECT id,email,password FROM users WHERE email = ?';
        //Put the parameters in an array
        $params = array($email);

        try {
            $statement = $pdo->prepare($query);
            $statement->execute($params);
            $result = $statement->fetch();
        } catch(PDOException $ex) {
            $result = $ex->getMessage();
            return $result;
        }

        if($statement->rowCount() === 1) {
            $existingUsr = true;
        } else {
            $existingUsr = false;
        }

        $result = ['data' => $result, 'exists' => $existingUsr];
        return $result;
    }

    /**
     * @return Boolean
     */
    private function checkPass($pass, $userData) {
        $hash = $userData['password'];
        if(password_verify($pass, $hash)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return result
     */
    private function setToken($token, $id) {
        $pdo = $this->getPDO();

        $query = "UPDATE users SET token = :token WHERE id = :id";

        try {
            $statement = $pdo->prepare($query);
            $statement->execute(array(':token'=>$token, ':id'=>$id));
        } catch(PDOException $ex) {
            $result = $ex->getMessage();
            return $result;
        }
    }

    /**
     * @return isExistingUser
     */
    public function logInUser($email, $password) {
        $isExistingUser = $this->checkExistenceOfUser($email);
        $validPas;

        if($isExistingUser['exists']) {
            $validPas = $this->checkPass($password, $isExistingUser['data']);
            $isExistingUser['validPas'] = $validPas;
        }

        if(!empty($validPas)) {
            // ob_start();
            $token = md5(uniqid(mt_rand(),true));
            $tabData = $isExistingUser['data'];

            $this->setToken($token, $tabData['id']);

            $expire = time()+3600*5; // 5 hours
            $id = $tabData['id'];
            $value = "token={$token},id={$id}";
            setcookie("gekkehenkie", $value, $expire, "/");
            // ob_end_flush();
        }

        return $isExistingUser;
    }

    /**
     * @return result
     */
    public function AuthCookie($token, $id) {
        $pdo = $this->getPDO();
        $result;
        //Query statement with placeholder
        $query = 'SELECT token FROM users WHERE id = ?';
        //Put the parameters in an array
        $params = array($id);

        try {
            $statement = $pdo->prepare($query);
            $statement->execute($params);
            $savedToken = $statement->fetch();
        } catch(PDOException $e) {
            $result = $sql . "<br>" . $e->getMessage();
        }

        if($savedToken['token'] === $token) {
            $newToken = md5(uniqid(mt_rand(),true));
            $expire = time()+3600*5; // 5 hours
            $value = "token={$newToken},id={$id}";
            setcookie("gekkehenkie", $value, $expire, "/");
            $this->setToken($newToken, $id);
            $result = true;
            return $result;
        } else {
            $result = false;
            return $result;
        }
    }

    // TODO MAKE OTHER RETURN DATA
    public function saveUsers($email, $password) {
        $isExistingUser = $this->checkExistenceOfUser($email);

        if(!$isExistingUser['exists']) {
            $pdo = $this->getPDO();
            try {
                $data = [$email, $password];
                $query = $sql = "
                INSERT INTO `users` (`email`, `password`)
                  VALUES(?, ?);
                INSERT INTO `userData` (`userID`)
                  VALUES(LAST_INSERT_ID());";

                  $statement = $pdo->prepare($query);
                  $status = $statement->execute($data);
             } catch(PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }

            if($status) {
                return "$status succes user created";
            } elseif (!$status) {
                return "$status failed something went wrong";
            }
        } else {
            return 'email already exists';
        }
    }
};
