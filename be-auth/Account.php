<?php
require __DIR__.'/config.php';
require __DIR__.'/Auth.php';
require __DIR__.'/AccDB.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ( ini_set( 'display_errors', '1' ) === false ) {
    return 'Unable to set display_errors.';
}

$userData = [];
$result;

if(!empty($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
}

if(!empty($_REQUEST['username'])) {
    $userName = $_REQUEST['username'];
    $userData['userName'] = $userName;
}

if(!empty($_REQUEST['userbio'])) {
    $userBio = $_REQUEST['userbio'];
    $userData['userBio'] = $userBio;
}

if(!empty($_REQUEST['userDOB'])) {
    $userDOB = $_REQUEST['userDOB'];
    $userData['userDOB'] = $userDOB;
}

class Account
{
    /**
     * @return response
     */
    private function getCookie() {
        if (isset($_COOKIE['gekkehenkie'])) {
            // get data from cookie for local use
            $pieces = explode(",", $_COOKIE["gekkehenkie"]); // holds an hash and a number
            $tokenHash = explode("=", $pieces[0])[1]; // hash
            $usrId = explode("=", $pieces[1])[1]; // number

            $config = new Config();
            $configuration =  $config->configuration;

            $auth = new Auth(
                $configuration['db_dsn'],
                $configuration['db_user'],
                $configuration['db_pass']
            );

            $response = $auth->AuthCookie($tokenHash, $usrId);
            return ['valid'=> $response, 'id' => $usrId];
        } else {
            // no cookie at all
            $response = false;
            return $response;
        }
    }

    public function saveAccountData($userData) {
        $validation = $this->getCookie();

        if ($validation['valid']) {

            $config = new Config();
            $configuration =  $config->configuration;

            $accDB = new AccDB(
                $configuration['db_dsn'],
                $configuration['db_user'],
                $configuration['db_pass']
            );
            $response = $accDB->saveUserInfo($userData, $validation['id']);

            return $response;
        } else {
            return 'Cookie monster!';
        }
    }

    public function accountData() {
        $validation = $this->getCookie();

        if ($validation['valid']) {
            $config = new Config();
            $configuration =  $config->configuration;

            $accDB = new AccDB(
                $configuration['db_dsn'],
                $configuration['db_user'],
                $configuration['db_pass']
            );
            $response = $accDB->getAccountData($validation['id']);
            return $response;
        }else {
            return 'Cookie monster!';
        }
    }
}

if(!empty($action)) {
    $call = new Account();
   $result = $call->$action($userData);
} else {
    $result = "I don't know what to do with myself!";
}

header('Access-Control-Allow-Origin: http://localhost:8080');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: Content-Type');

if (!is_string($result)) {
    echo json_encode($result);
} else {
    echo $result;
}
