<?php
require __DIR__.'/config.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if ( ini_set( 'display_errors', '1' ) === false ) {
    throw new \Exception( 'Unable to set display_errors.' );  
}

class Pdo 
{
    private $pdo;
    private $dbDsn;
    private $dbUser;
    private $dbPass;

    public function __construct() {
        $config = new Config();
        $configuration =  $config->configuration;
        
        $this->dbDsn = $configuration['db_dsn'];
        $this->dbUser = $configuration['db_user'];
        $this->dbPass = $configuration['db_pass'];
    }

    /**
     * @return PDO
     */

    private function makePDO()
    {
        if(null === $this->pdo) {
            $pdo = new PDO($this->dbDsn, $this->dbUser, $this->dbPass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }

        return $this->pdo;
    }

    public function makeConnection() {
        $response = $this->makePDO();
        return $response;
    }
}