<?php

class AccDB
{
    private $pdo;
    private $dbDsn;
    private $dbUser;
    private $dbPass;

    public function __construct($dbDsn, $dbUser, $dbPass) {
        $this->dbDsn = $dbDsn;
        $this->dbUser = $dbUser;
        $this->dbPass = $dbPass;
    }

    /**
     * @return PDO
     */

    private function getPDO()
    {
        if(null === $this->pdo) {
            $pdo = new PDO($this->dbDsn, $this->dbUser, $this->dbPass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }

        return $this->pdo;
    }


    public function saveUserInfo($userData, $id) {
        if(!empty($userData['userName'])) {
            $name = $userData['userName'];
        } else {
            $name = 'NULL';
        }

        if(!empty($userData['userBio'])) {
            $bio = $userData['userBio'];
        } else {
            $bio = 'NULL';
        }

        if(!empty($userData['userDOB'])) {
            $dob = $userData['userDOB'];
        } else {
            $dob = 'NULL';
        }

        $data = [$name,$bio,$dob,$id];
        $query = "UPDATE `userData` SET `userName`=?, `userBio`=?, `userDOB`=? WHERE `userID`=?";

        $pdo = $this->getPDO();
        $statement = $pdo->prepare($query);
        $response = $statement->execute($data);

        if ($response) {
            return 'Your info is saved';
        } else {
            return 'We could not update your info';
        }
    }

    public function getAccountData($id) {
        $data = [$id];
        $query = "SELECT * FROM `userData` WHERE `userId`=?";

        $pdo = $this->getPDO();
        $statement = $pdo->prepare($query);
        $statement->execute($data);
        $usersInfoData = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $usersInfoData;
    }
}
