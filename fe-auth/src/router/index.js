import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import store from '@/store'
import Home from '@/pages/Home'
import Welcome from '@/pages/Welcome'
import myInfo from '@/pages/MyInfo'

Vue.use(Router)


const authenticated = store.getters.authenticated

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '*',
            name: 'not-existing',
            redirect: '/home',
        },
        {
            path: '/',
            name: 'Any',
            redirect: '/home',
        },
        {
            path: '/home',
            name: 'Home',
            component: Home,
        },
        {
            path: '/welcome',
            name: 'welcome',
            component: Welcome,
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/my-info',
            name: 'my-info',
            component: myInfo,
            meta: {
                requiresAuth: true,
            }
        },
    ]
})

router.beforeEach((to, from, next) => {
    const isAuthRequired = (typeof to.meta.requiresAuth !== 'undefined') ? to.meta.requiresAuth : false;

    if (isAuthRequired) {
        axios.defaults.withCredentials = true;
        axios.get('http://localhost:8000/Users.php?action=getCookie').then(function(result) {
            const isValid = result.data;
            
            if (!isValid) {
                document.cookie = 'gekkehenkie=; Max-Age=-99999999;';
                next(false);
                store.commit('authentication', false);
                if (to.path !== '/home') {
                    router.push({name:"Home"});
                }
            } else {
                store.commit('authentication', true);
                next();
            }

        }).catch(function(error) {
            console.log(error);
        });
    } else {
        next();
    }    
})

export default router;