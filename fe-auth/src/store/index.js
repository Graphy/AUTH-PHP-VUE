import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        activeLogin: false,
        activeSignup: false,
        authenticated: false,
    },
    mutations: {
        activateLogin(state, payload) {
            state.activeLogin = payload;
        },
        activateSignup(state, payload) {
            state.activeSignup = payload;
        },
        authentication(state, payload) {
            state.authenticated = payload;
        }
    },
    getters: {
        activeLogin: state => {
            return state.activeLogin;
        },
        activeSignup: state => {
            return state.activeSignup;
        },
        authenticated: state => {
            return state.authenticated;
        },
    }
})

export default store;